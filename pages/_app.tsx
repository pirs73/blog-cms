import type { AppProps } from 'next/app';
import ErrorBoundary from '@app/hoc/ErrorBoundary';
import '../assets/css/styles.css';

import type { JSX } from 'react';

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <ErrorBoundary>
      <Component {...pageProps} />
    </ErrorBoundary>
  );
};

export default MyApp;
