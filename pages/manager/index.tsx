import { useEffect } from 'react';
import { pathNameVar } from '../../cache';
import { AdminLayout } from '../../layouts';

const Manager = () => {
  useEffect(() => {
    pathNameVar('');
  }, []);

  return (
    <AdminLayout title="Site Title">
      <h1>Dashboard</h1>
      <ul>
        <li>Posts: 200</li>
        <li>Media: 210</li>
      </ul>
    </AdminLayout>
  );
};

export default Manager;
