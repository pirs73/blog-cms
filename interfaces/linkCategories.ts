import { ILink } from '@app/interfaces';
export interface LinkCategoryFields {
  name: string;
  description?: string;
  slug?: string;
  links?: ILink[];
}
export interface LinkCategory extends LinkCategoryFields {
  _id: string;
}

export interface LinkCategories {
  linkCategories: LinkCategory[];
}
