export const userTypes = `
  enum RoleTypes {
    guest
    admin
    manager
  }

  type User {
    _id: ID!
    avatar: String
    username: String!
    name: String
    email: String!
    role: RoleTypes!
    createdAt: String
  }

  input SignUpInput {
    avatar: String
    username: String!
    name: String
    email: String!
    password: String!
    passwordConfirmation: String!
  }

  input SignInInput {
    email: String!
    password: String!
  }

  type SignUpPayload {
    user: User!
  }

  type SignInPayload {
    user: User!
  }

  type ViewerResponse {
    token: String
    user: User
  }
`;
