import { ReactNode, useEffect, useState, type JSX } from 'react';
import Link from 'next/link';
import cn from 'classnames';
import { AppNavbar } from '@app/components/';
import { useLazyViewerQuery } from '@app/apollo/actions';
import { connectWithSocketServer } from '@app/realtimeCommunication/socketConnection';
import { Viewer } from '@app/interfaces';

import styles from './TopMenu.module.css';

type AppLinkProps = {
  children?: ReactNode;
  className?: string;
  href: string;
};

const AppLink = ({ children, className, href }: AppLinkProps): JSX.Element => (
  <Link href={href} className={className}>
    {children}
  </Link>
);

const TopMenu = () => {
  const [viewer, setViewer] = useState<Viewer | null>(null);
  const [hasResponse, setHasResponse] = useState(false);
  const [topMenuShow, setTopMenuShow] = useState<boolean>(false);
  const [dropdownShow, setDropdownShow] = useState<boolean>(false);

  const [viewerQuery, { data, error }] = useLazyViewerQuery();

  useEffect(() => {
    viewerQuery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (error) {
    return <div>Error...</div>;
  }

  if (data) {
    if (data.viewer && !viewer) {
      setViewer(data.viewer.user);
      // connectWithSocketServer();
      connectWithSocketServer(data.viewer?.token);
    }
    if (!data.viewer && viewer) {
      setViewer(null);
    }
    if (!hasResponse) {
      setHasResponse(true);
    }
  }

  return (
    <div className={styles.topMenu}>
      <AppNavbar>
        <AppLink href="/" className={styles.brand}>
          Site Title
        </AppLink>
        <button
          className={styles.toggleButton}
          onClick={() => setTopMenuShow(!topMenuShow)}
        >
          <span className={styles.toggleIcon}></span>
        </button>
        <div
          className={cn(styles.navbarCollapse, {
            [styles.navbarOpened]: topMenuShow,
          })}
        >
          <div className={styles.navbar}>
            <AppLink className={styles.navLink} href="/posts/">
              Posts
            </AppLink>
            <AppLink className={styles.navLink} href="/forum/categories/">
              Forum
            </AppLink>
          </div>
          <form className={styles.searchForm}>
            <input
              className={styles.searchInput}
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button className={styles.searchButton} type="submit">
              Search
            </button>
          </form>
          {hasResponse && (
            <div className={styles.rigthNavBar}>
              {viewer && (
                <>
                  <span className={styles.navLink}>
                    Welcome {viewer.username}
                  </span>
                  {(viewer.role === 'admin' || viewer.role === 'manager') && (
                    <div className={styles.dropdown}>
                      <button
                        className={styles.dropdownToggle}
                        onClick={() => setDropdownShow(!dropdownShow)}
                      >
                        Manage
                      </button>
                      {dropdownShow && (
                        <div className={styles.dropdownMenu}>
                          <AppLink
                            href="/manager/"
                            className={styles.dropdownItem}
                          >
                            Dashboard
                          </AppLink>
                        </div>
                      )}
                    </div>
                  )}
                  <AppLink href="/logout" className={styles.logoutButton}>
                    Logout
                  </AppLink>
                </>
              )}
              {!viewer && (
                <div className={styles.navbar}>
                  <AppLink className={styles.navLink} href="/login/">
                    Sign In
                  </AppLink>
                  <AppLink className={styles.navLink} href="/register/">
                    Sign Up
                  </AppLink>
                </div>
              )}
            </div>
          )}
        </div>
      </AppNavbar>
    </div>
  );
};

export { TopMenu };
