import { PostCategoryResponse, TagResponse } from '@app/interfaces';

export interface PostFields {
  title: string;
  content: any;
  categories?: PostCategoryResponse[];
  tags?: TagResponse[];
  published: boolean;
  postedBy: string;
  slug: string;
}

export interface PostResponse extends PostFields {
  _id: string;
  createdAt: string;
  updatedAt: string;
}

export interface Posts {
  posts: PostResponse[];
}
