import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const Tags = () => {
  useEffect(() => {
    pathNameVar('/manager/tags/');
  }, []);

  return (
    <AdminLayout title="Tags">
      <h1>Tags</h1>
    </AdminLayout>
  );
};

export default Tags;
