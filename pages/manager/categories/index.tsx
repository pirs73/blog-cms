import React, { useEffect } from 'react';
import withApollo from '@app/hoc/withApollo';
import { AdminLayout } from '@app/layouts';
import { pathNameVar } from '@app/cache';
import { PostCategories } from '@app/view';

const PostCategoriesPage = () => {
  useEffect(() => {
    pathNameVar('/manager/categories/');
  }, []);

  return (
    <AdminLayout title="Posts Categories">
      <PostCategories />
    </AdminLayout>
  );
};

export default withApollo(PostCategoriesPage);
