import React from 'react';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import * as z from 'zod';
import cn from 'classnames';
import { useCreatePostCategory } from '@app/apollo/actions';
import { PostCategoryFields } from '@app/interfaces';
import { CratePostCategoryFormProps } from './CratePostCategoryForm.props';
import styles from './CratePostCategoryForm.module.css';

const schema = z.object({
  name: z
    .string({
      required_error: 'Name is required',
      invalid_type_error: 'Name must be a string',
    })
    .min(2, { message: 'Must be 2 or more characters long' }),
  slug: z
    .string({
      required_error: 'Slug is required',
      invalid_type_error: 'Slug must be a string',
    })
    .min(2, { message: 'Must be 2 or more characters long' }),
});

const CratePostCategoryForm = ({
  className,
  postCategories,
  ...props
}: CratePostCategoryFormProps) => {
  const [createPostCategory, { error }] = useCreatePostCategory();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<PostCategoryFields>({
    mode: 'onBlur',
    resolver: zodResolver(schema),
    defaultValues: {},
  });

  const onSubmit = async (data: PostCategoryFields) => {
    if (data.parentCategory === '') {
      data.parentCategory = null;
    }

    const { name, slug, parentCategory, description } = data;

    try {
      await createPostCategory({
        variables: { name, slug, parentCategory, description },
      });
    } catch (error) {
      console.error(error);
    }
    reset();
  };

  if (error) {
    return (
      <div>
        <p>{error.toString()}</p>
      </div>
    );
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} {...props} className={className}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input
          {...register('name')}
          type="text"
          className="form-control"
          id="name"
          required
        />
        {errors && (
          <p className={styles.errorMessage}>
            {(errors.name && errors.name.message)?.toString()}
          </p>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="slug">Slug</label>
        <input
          {...register('slug')}
          type="text"
          className="form-control"
          id="slug"
          required
        />
        {errors && (
          <p className={styles.errorMessage}>
            {(errors.slug && errors.slug.message)?.toString()}
          </p>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="parentCategory">Parent Category</label>
        <select
          {...register('parentCategory')}
          className="form-control"
          id="parentCategory"
        >
          <option value=""></option>
          {postCategories &&
            postCategories.map((el) => (
              <option key={el._id} value={el._id}>
                {el.name}
              </option>
            ))}
        </select>
      </div>
      <div className="form-group mt-3">
        <label htmlFor="description">Description</label>
        <input
          {...register('description')}
          type="text"
          className="form-control"
          id="description"
        />
      </div>
      <button type="submit" className={cn(`btn btn-primary`, styles.button)}>
        Submit
      </button>
    </form>
  );
};

export default CratePostCategoryForm;
