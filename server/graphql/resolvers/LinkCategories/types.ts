import { LinkCategory } from '../../../../interfaces';

export interface LinkCategoryInputArgs {
  id?: string;
  input: LinkCategory;
}

export interface LinkCategoryArgs {
  id: string;
}
