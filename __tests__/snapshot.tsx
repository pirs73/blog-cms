/**
 * @jest-environment jsdom
 */
import { render } from '@testing-library/react';
import IndexPage from '../pages';

it('renders homepage unchanged', () => {
  const { container } = render(<IndexPage />);
  expect(container).toMatchSnapshot();
});
