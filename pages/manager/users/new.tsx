import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const UserCreate = () => {
  useEffect(() => {
    pathNameVar('/manager/users/new/');
  }, []);

  return (
    <AdminLayout title="Create User">
      <h1>Create User</h1>
    </AdminLayout>
  );
};

export default UserCreate;
