import React, { useEffect } from 'react';
import { AdminLayout } from '../../../layouts';
import { pathNameVar } from '../../../cache';

const ForumCategories = () => {
  useEffect(() => {
    pathNameVar('/manager/forum/categories/');
  }, []);

  return (
    <AdminLayout title="Forum Categories">
      <h1>Forum Categories</h1>
    </AdminLayout>
  );
};

export default ForumCategories;
