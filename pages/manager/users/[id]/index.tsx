 
import React, { useEffect } from 'react';
import { pathNameVar } from '@app/cache';
import { AdminLayout } from '@app/layouts';

const UserProfile = () => {
  useEffect(() => {
    pathNameVar('/manager/users/[id]/');
  }, []);

  return (
    <AdminLayout title="User Profile">
      <h1>User Profile</h1>
    </AdminLayout>
  );
};

export async function getStaticPaths() {
  return {
    paths: [
      // String variant:
      '/manager/users/1',
      // Object variant:
      { params: { id: '2' } },
    ],
    fallback: true,
  };
}

export async function getStaticProps(_context: any) {
  return {
    props: {}, // will be passed to the page component as props
  };
}

export default UserProfile;
