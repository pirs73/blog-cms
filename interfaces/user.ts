export enum RoleTypes {
  Guest = 'guest',
  Admin = 'admin',
  Manager = 'manager',
}

export interface SignUpDataArgs {
  avatar?: string;
  username: string;
  name?: string;
  email: string;
  password: string;
  passwordConfirmation: string;
}

export interface SignUpInputArgs {
  input: SignUpDataArgs;
}

export interface SignInDataArgs {
  email: string;
  password: string;
}
export interface SignInInputArgs {
  input: SignInDataArgs;
}

export interface IUser {
  _id: string | unknown;
  avatar?: string;
  username: string;
  password: string;
  salt: string;
  name?: string;
  email: string;
  role: RoleTypes;
  createdAt?: string;
  updatedAt?: string;
}

export interface Viewer {
  _id: string;
  email: string;
  username: string;
  role: string;
}
