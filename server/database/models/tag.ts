import mongoose from 'mongoose';

const { Schema } = mongoose;

export interface ITag extends mongoose.Document {
  name: string;
  description?: string;
  slug: string;
}

const tagSchema = new Schema({
  name: { type: String, required: true, maxlength: 128 },
  description: { type: String, maxlength: 512 },
  slug: { type: String, required: true, maxlength: 64 },
});

export default mongoose.model<ITag>('Tag', tagSchema);
