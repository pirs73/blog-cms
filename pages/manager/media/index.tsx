import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const MediaLibrary = () => {
  useEffect(() => {
    pathNameVar('/manager/media/');
  }, []);

  return (
    <AdminLayout title="Media Library">
      <h1>Media Library</h1>
    </AdminLayout>
  );
};

export default MediaLibrary;
