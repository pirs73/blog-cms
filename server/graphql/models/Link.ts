class Link {
  [x: string]: any;
  constructor(model: any) {
    this.Model = model;
  }
  async getAll() {
    return await this.Model.find({}).populate('category');
  }
}

export { Link };
