/* eslint-disable @typescript-eslint/no-unused-vars */
import { IUser, SignUpDataArgs, SignInDataArgs } from '../../../interfaces';
class User {
  [x: string]: any;
  constructor(model: any) {
    this.Model = model;
  }

  // getAuthUser(ctx: any): IUser | null {
  //   if (ctx.isAuthenticated()) {
  //     return ctx.getUser();
  //   }

  //   return null;
  // }

  async getAllUsers(): Promise<IUser[]> {
    try {
      return await this.Model.find({});
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  // async signUp(signUpData: SignUpDataArgs) {
  //   if (signUpData.password !== signUpData.passwordConfirmation) {
  //     throw new Error('Password must be the same as confirmation password!');
  //   }

  //   try {
  //     return await this.Model.create(signUpData);
  //   } catch (error: any) {
  //     if (error && error.code && error.code === 11000) {
  //       throw new Error('User with provided email already exists!');
  //     }

  //     throw error;
  //   }
  // }

  // async signIn(signInData: SignInDataArgs, ctx: any) {
  //   try {
  //     const user = ctx.authenticate(signInData);
  //     return user;
  //   } catch (error) {
  //     return error;
  //   }
  // }

  // signOut(ctx: any) {
  //   try {
  //     ctx.logout();
  //     return true;
  //   } catch (error) {
  //     return false;
  //   }
  // }
}

export { User };
