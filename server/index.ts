import { expressMiddleware } from '@apollo/server/express4';
import express, { Express } from 'express';
import * as http from 'http';
import next, { NextApiHandler, NextApiRequest } from 'next';
import mongoose from 'mongoose';
import { connectDatabase } from './database';
import { apolloServer } from './graphql';
import { init } from './middlewares';
import { Link, LinkCategory, PostCategory, Tag, User } from './graphql/models';

const port: number = parseInt(process.env.PORT || '3000', 10);
const dev: boolean = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const nextHandler: NextApiHandler = nextApp.getRequestHandler();

nextApp.prepare().then(async () => {
  await connectDatabase();

  const app: Express = express();
  const server: http.Server = http.createServer(app);

  init(app);

  const serverApollo = apolloServer(server);

  await serverApollo.start();

  app.use(
    '/graphql',
    // expressMiddleware accepts the same arguments:
    // an Apollo Server instance and optional configuration options
    expressMiddleware(serverApollo, {
      context: async ({ req, res }) => ({
        res: res,
        req: req,
        token: req.headers.token,
        models: {
          LinkCategory: new LinkCategory(mongoose.model('LinkCategory')),
          Link: new Link(mongoose.model('Link')),
          PostCategory: new PostCategory(mongoose.model('PostCategory')),
          Tag: new Tag(mongoose.model('Tag')),
          User: new User(mongoose.model('User')),
        },
      }),
    }),
  );

  app.all('*', (req: NextApiRequest, res: any) => {
    return nextHandler(req, res);
  });

  await new Promise<void>((resolve) => server.listen({ port }, resolve));

  console.info(`🚀 Server ready at http://localhost:${port}`);
});
