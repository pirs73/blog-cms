import { useEffect } from 'react';
import { useRouter } from 'next/router';

interface RedirectProps {
  to: string;
}

const Redirect = ({ to }: RedirectProps) => {
  const router = useRouter();
  useEffect(() => {
    router.push({ pathname: to });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export { Redirect };
