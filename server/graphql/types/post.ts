const PostCategoryFields = `
  name: String!
  slug: String!
  parentCategory: String
  description: String
`;

const PostFields = `
  title: String!
  content: String!
  categories: [String]
  tags: [String]
  published: Boolean!
  postedBy: String!
  slug: String!
`;

const TagFields = `
  name: String!
  description: String
  slug: String!
`;

export const postTypes = `
  input PostCategoryInput {
    ${PostCategoryFields}
  }

  type PostCategoryResponse {
    _id: ID!
    ${PostCategoryFields}
    createdAt: String
    updatedAt: String
  }

  input PostInput {
    ${PostFields}
  }

  type PostResponse {
    _id: ID!
    ${PostFields}
    createdAt: String
    updatedAt: String
  }

  input TagInput {
    ${TagFields}
  }

  type TagResponse {
    _id: ID!
    ${TagFields}
  }
`;
