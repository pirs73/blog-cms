import { useQuery, useMutation } from '@apollo/client';
import { GET_POST_CATEGORIES, CREATE_POST_CATEGORY } from '@app/apollo/queries';

export const useGetPostCategories = () => useQuery(GET_POST_CATEGORIES);

export const useCreatePostCategory = () =>
  useMutation(CREATE_POST_CATEGORY, {
    refetchQueries: [GET_POST_CATEGORIES],
    // update(cache: any, { data: { createLinkCategory } }: any) {
    //   if (cache.data.data.ROOT_QUERY.linkCategories) {
    //     const { linkCategories }: any = cache.readQuery({
    //       query: GET_LINK_CATEGORIES,
    //     });
    //     cache.writeQuery({
    //       query: GET_LINK_CATEGORIES,
    //       data: { linkCategories: [...linkCategories, createLinkCategory] },
    //     });
    //   }
    // },
  });

// export const useUpdateLinkCategory = () => useMutation(UPDATE_LINK_CATEGORY);

// export const useDeleteLinkCategory = () =>
//   useMutation(DELETE_LINK_CATEGORY, {
//     update(cache: any, { data: { deleteLinkCategory } }: any) {
//       if (cache.data.data.ROOT_QUERY.linkCategories) {
//         const { linkCategories }: any = cache.readQuery({
//           query: GET_LINK_CATEGORIES,
//         });
//         const newLinkCategories = linkCategories.filter(
//           (lc: any) => lc._id !== deleteLinkCategory,
//         );
//         cache.writeQuery({
//           query: GET_LINK_CATEGORIES,
//           data: { linkCategories: newLinkCategories },
//         });
//       }
//     },
//   });
