import React, { useEffect } from 'react';
import withApollo from '@app/hoc/withApollo';
import {
  useGetLinkCategories,
  useUpdateLinkCategory,
  useDeleteLinkCategory,
} from '@app/apollo/actions';
import { pathNameVar } from '@app/cache';
import { AdminLayout } from '@app/layouts';
import { AdminLinkCategories } from '@app/view';

const LinkCategoriesPage = () => {
  const { error, data } = useGetLinkCategories();

  const [updateLinkCategory] = useUpdateLinkCategory();
  const [deleteLinkCategory] = useDeleteLinkCategory();

  useEffect(() => {
    pathNameVar('/manager/links/categories/');
  }, []);

  const linkCategories = (data && data.linkCategories) || [];

  if (error) return <div>`Error! ${error.message}`</div>;

  return (
    <AdminLayout title="Link Categories">
      <h1>Link Categories</h1>

      <AdminLinkCategories
        updateLinkCategory={updateLinkCategory}
        deleteLinkCategory={deleteLinkCategory}
        data={linkCategories}
      />
    </AdminLayout>
  );
};

export default withApollo(LinkCategoriesPage);
