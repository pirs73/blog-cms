import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { useApolloClient } from '@apollo/client';
import { getErrorMessage } from '../lib/form';
import withApollo from '../hoc/withApollo';
import { BaseLayout } from '../layouts';
import { LoginForm } from '../components';
import { SignInDataArgs } from '../interfaces';
import { useSignIn } from '../apollo/actions';

const Login = () => {
  const [signIn] = useSignIn();
  const client = useApolloClient();
  const [errorMsg, setErrorMsg] = useState();
  const router = useRouter();

  const handleSubmit = async (signInData: SignInDataArgs) => {
    const { email, password } = signInData;
    try {
      await client.resetStore();
      const { data } = await signIn({
        variables: { email, password },
      });
      if (data.signIn.user) {
        await router.push('/');
      }
    } catch (error) {
      setErrorMsg(getErrorMessage(error));
    }
  };

  return (
    <BaseLayout title="Login">
      <section className="container">
        <h1 className="pt-3 pb-3 text-center">Login Page</h1>
        <div className="row">
          <div className="col-md-6 mx-auto">
            <LoginForm
              onSubmit={(signInData: SignInDataArgs) =>
                handleSubmit(signInData)
              }
            />
            {errorMsg && <div className="alert alert-danger">{errorMsg}</div>}
          </div>
        </div>
      </section>
    </BaseLayout>
  );
};

export default withApollo(Login);
