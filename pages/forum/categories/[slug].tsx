import { BaseLayout } from '../../../layouts';

const Topics = () => {
  return (
    <BaseLayout title="Topics">
      <h1>Topics</h1>
    </BaseLayout>
  );
};

export default Topics;
