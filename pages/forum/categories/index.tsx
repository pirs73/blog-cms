import { BaseLayout } from '../../../layouts';

const ForumCategories = () => {
  return (
    <BaseLayout title="Forum Categories">
      <h1>Forum Categories</h1>
    </BaseLayout>
  );
};

export default ForumCategories;
