import { useMutation } from '@apollo/client';
import withApollo from '../hoc/withApollo';
import { SIGN_UP } from '../apollo/queries';
import { BaseLayout } from '../layouts';
import { Redirect, RegisterForm } from '../components';

const Register = () => {
  const [signUp, { data, error }] = useMutation(SIGN_UP);
  const errorMessage = (error: any) => {
    return (
      (error.graphQLErrors && error.graphQLErrors[0]?.message) ||
      'Ooooops something went wrong...'
    );
  };

  return (
    <BaseLayout title="Sign Up">
      <section className="container">
        <h1 className="pt-3 pb-3 text-center">Register</h1>
        <div className="row">
          <div className="col-md-6 mx-auto">
            <RegisterForm
              onSubmit={(registerData: any) =>
                signUp({ variables: registerData })
              }
            />
            {data && data.signUp && <Redirect to="/login/" />}
            {error && (
              <div className="alert alert-danger" role="alert">
                {errorMessage(error)}
              </div>
            )}
          </div>
        </div>
      </section>
    </BaseLayout>
  );
};

export default withApollo(Register);
