import { useQuery, useMutation } from '@apollo/client';
import {
  GET_LINK_CATEGORIES,
  CREATE_LINK_CATEGORY,
  UPDATE_LINK_CATEGORY,
  DELETE_LINK_CATEGORY,
} from '@app/apollo/queries';

export const useGetLinkCategories = () => useQuery(GET_LINK_CATEGORIES);

export const useCreateLinkCategory = () =>
  useMutation(CREATE_LINK_CATEGORY, {
    update(cache: any, { data: { createLinkCategory } }: any) {
      if (cache.data.data.ROOT_QUERY.linkCategories) {
        const { linkCategories }: any = cache.readQuery({
          query: GET_LINK_CATEGORIES,
        });
        cache.writeQuery({
          query: GET_LINK_CATEGORIES,
          data: { linkCategories: [...linkCategories, createLinkCategory] },
        });
      }
    },
  });

export const useUpdateLinkCategory = () =>
  useMutation(UPDATE_LINK_CATEGORY, {
    refetchQueries: [GET_LINK_CATEGORIES],
  });

export const useDeleteLinkCategory = () =>
  useMutation(DELETE_LINK_CATEGORY, {
    refetchQueries: [GET_LINK_CATEGORIES],
  });
