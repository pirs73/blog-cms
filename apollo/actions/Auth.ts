import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { GET_USER, SIGN_IN, SIGN_OUT, VIEWER_QUERY } from '../queries';

export const useSignIn = () =>
  useMutation(SIGN_IN, {
    refetchQueries: [GET_USER, VIEWER_QUERY, 'User', 'Viewer'],
  });

export const useSignOut = () => useMutation(SIGN_OUT);

export const useLazyGetUser = () => useLazyQuery(GET_USER);

export const useGetUser = (_args: any) => useQuery(GET_USER);

export const useLazyViewerQuery = () => useLazyQuery(VIEWER_QUERY);
