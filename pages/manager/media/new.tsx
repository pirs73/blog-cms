import React, { useEffect } from 'react';
import withApollo from '@app/hoc/withApollo';
import { pathNameVar } from '@app/cache';
import { AdminLayout } from '@app/layouts';
import { FileUpload } from '@app/components';

const MediaCreate = () => {
  useEffect(() => {
    pathNameVar('/manager/media/new/');
  }, []);

  return (
    <AdminLayout title="Media Create">
      <h1>Media Create</h1>
      <FileUpload />
    </AdminLayout>
  );
};

export default withApollo(MediaCreate);
