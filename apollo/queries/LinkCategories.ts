import { gql } from '@apollo/client';

export const GET_LINK_CATEGORIES = gql`
  query LinkCategories {
    linkCategories {
      _id
      name
      description
      slug
      links {
        _id
        title
        url
      }
    }
  }
`;

export const CREATE_LINK_CATEGORY = gql`
  mutation CreateLinkCategory(
    $name: String
    $description: String
    $slug: String
  ) {
    createLinkCategory(name: $name, description: $description, slug: $slug) {
      _id
      name
      description
      slug
    }
  }
`;

export const UPDATE_LINK_CATEGORY = gql`
  mutation UpdateLinkCategory($id: ID) {
    updateLinkCategory(
      id: $id
      input: {
        name: "Up Link Category"
        description: "Up Link Category Desc"
        slug: "Up-slug"
      }
    )
    # {
    #   # _id
    #   # name
    #   # description
    #   # slug
    # }
  }
`;

export const DELETE_LINK_CATEGORY = gql`
  mutation DeleteLinkCategory($id: ID) {
    deleteLinkCategory(id: $id)
  }
`;
