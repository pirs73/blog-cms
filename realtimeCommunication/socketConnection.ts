import io, { Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import { newPostCreateVar } from '@app/cache';

let socket: Socket<DefaultEventsMap, DefaultEventsMap> | null = null;

export const connectWithSocketServer = (token: string) => {
  socket = io('http://localhost:5002', {
    auth: {
      token,
    },
  });

  socket?.on('connect', () => {
    console.info('successfully connected with socket.io server');
    console.info(socket?.id);
  });

  socket?.on('new-post', (data) => {
    const { newPostCreate } = data;

    console.info('new post event came');
    console.info(newPostCreate);

    newPostCreateVar(newPostCreate);
  });
};
