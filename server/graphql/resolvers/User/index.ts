import { GraphQLError } from 'graphql';
import { createUser, findUser, validatePassword } from '../../../../lib/user';
import { setLoginSession, getLoginSession } from '../../../../lib/auth';
import { removeTokenCookie } from '../../../../lib/auth-cookies';
import {
  IUser,
  SignUpInputArgs,
  SignInInputArgs,
} from '../../../../interfaces';

export const userQueries = {
  viewer: async (
    _root: undefined,
    _args: undefined,
    context: any,
    _info: any,
  ): Promise<IUser | any> => {
    try {
      const sessionRes = await getLoginSession(context.req);

      if (sessionRes) {
        const { session, token } = sessionRes;

        const user = await findUser({ email: session.email });
        return {
          user,
          token,
        };
      }
    } catch (error) {
      console.error(error);
      throw new GraphQLError('Authentication token is invalid, please log in', {
        extensions: {
          code: 'UNAUTHENTICATED',
        },
      });
    }
  },

  users: async (
    _root: undefined,
    _args: any,
    context: any,
    _info: any,
  ): Promise<IUser[] | any> => {
    return await context.models.User.getAllUsers();
  },
};

export const userMutations = {
  signUp: async (
    _root: undefined,
    { input }: SignUpInputArgs,
    _context: any,
    _info: any,
  ): Promise<any> => {
    const user = await createUser(input);
    return { user };
  },
  signIn: async (
    _root: undefined,
    { input }: SignInInputArgs,
    context: any,
    _info: any,
  ): Promise<any> => {
    const user = await findUser({ email: input.email });

    if (user && (await validatePassword(user, input.password))) {
      const session = {
        id: user._id,
        email: user.email,
      };

      await setLoginSession(context.res, session);

      return { user };
    } else {
      throw new GraphQLError('Invalid email and password combination');
    }
  },
  signOut: async (
    _root: undefined,
    _args: any,
    context: any,
    _info: any,
  ): Promise<boolean> => {
    removeTokenCookie(context.res);
    return true;
  },
};
