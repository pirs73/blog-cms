import { gql } from '@apollo/client';

const postCategoryResponse = `
      _id
      name
      slug
      parentCategory
      description
      createdAt
      updatedAt
`;

export const GET_POST_CATEGORIES = gql`
  query PostCategories {
    postCategories {
      ${postCategoryResponse}
    }
  }
`;

export const CREATE_POST_CATEGORY = gql`
  mutation CreatePostCategory(
    $name: String!
    $slug: String!
    $parentCategory: String
    $description: String
  ) {
    createPostCategory(
      name: $name
      slug: $slug
      parentCategory: $parentCategory
      description: $description
    ) {
     ${postCategoryResponse}
    }
  }
`;

// export const UPDATE_LINK_CATEGORY = gql`
//   mutation UpdateLinkCategory($id: ID) {
//     updateLinkCategory(
//       id: $id
//       input: {
//         name: "Up Link Category"
//         description: "Up Link Category Desc"
//         slug: "Up-slug"
//       }
//     ) {
//       _id
//       name
//       description
//       slug
//     }
//   }
// `;

// export const DELETE_LINK_CATEGORY = gql`
//   mutation DeleteLinkCategory($id: ID) {
//     deleteLinkCategory(id: $id)
//   }
// `;
