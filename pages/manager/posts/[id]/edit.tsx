import React, { useEffect } from 'react';
import { pathNameVar } from '../../../../cache';
import { AdminLayout } from '../../../../layouts';

const PostEdit = () => {
  useEffect(() => {
    pathNameVar('/manager/posts/');
  }, []);

  return (
    <AdminLayout title="Post Edit">
      <h1>Post Edit</h1>
    </AdminLayout>
  );
};

export default PostEdit;
