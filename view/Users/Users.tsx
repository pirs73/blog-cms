/* eslint-disable @next/next/no-img-element */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { CheckCircleTwoTone } from '@ant-design/icons';
import { Button, Table } from 'antd';
import { useGetAllUsers } from '@app/apollo/actions';
import { IUser } from '@app/interfaces';

const useInitialData = () => {
  const { error, data } = useGetAllUsers();

  const usersItems = (data && data.users) || [];

  return {
    usersItems,
  };
};

const Users = () => {
  const { usersItems: data } = useInitialData();

  const columns = [
    {
      title: 'Avatar',
      key: 'avatar',
      render: (_text: any, record: { avatar: string }) => (
        <img src={record.avatar} alt="Avatar" width="30px" height="30px" />
      ),
    },
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: 'Online',
      key: 'online',
      render: (_text: any) => (
        <div>{true && <CheckCircleTwoTone twoToneColor="#52c41a" />}</div>
      ),
    },
  ];

  return (
    <div>
      <h1>All Users</h1>
      <div>
        {data && data.length > 0 && (
          <Table
            columns={columns}
            dataSource={data}
            rowKey="_id"
            pagination={false}
          />
        )}
      </div>
    </div>
  );
};

export default Users;
