import withApollo from '../hoc/withApollo';
import withAuth from '@app/hoc/withAuth';
import { BaseLayout } from '../layouts';

const Secret = () => {
  return (
    <BaseLayout title="Secret">
      <section className="container">
        <h1 className="pt-3 pb-3 text-center">Secret Page</h1>
        <div className="row">
          <div className="col-md-6 mx-auto">
            SECRET PAGE
            <br />
          </div>
        </div>
      </section>
    </BaseLayout>
  );
};

export default withApollo(withAuth(Secret, 'admin'));
