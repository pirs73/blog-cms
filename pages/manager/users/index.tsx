import React, { useEffect } from 'react';
import withApollo from '@app/hoc/withApollo';
import { pathNameVar } from '@app/cache';
import { AdminLayout } from '@app/layouts';
import { Users } from '@app/view';

const UsersPage = () => {
  useEffect(() => {
    pathNameVar('/manager/users/');
  }, []);

  return (
    <AdminLayout title="Users">
      <Users />
    </AdminLayout>
  );
};

export default withApollo(UsersPage);
