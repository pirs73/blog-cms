# Blog CMS

This is a really simple project that shows the usage of Next.js with TypeScript.

## Install

engines: <br />
node: ">=18.17", <br />
npm: ">=9"

### `npm install`

## Run development mode

### `npm run dev`

## Run production mode

### `npm run build`

### `npm run start`

<!-- ## Podman

```bash
$ podman build --file=blogcms.containerfile -t blogcms .
$ podman build --file=socket.containerfile -t socket .
$ podman-compose up -d
``` -->

## Backup and restore DB

```bash
$ mongodump --db=blogcmsDB --uri="mongodb://127.0.0.1:27017"
$ mongorestore --drop dump/
windows
.\mongodump --db=blogcmsDB --uri="mongodb://127.0.0.1:27017"
.\mongorestore --drop dump/
```

<br />
t4.3.4
