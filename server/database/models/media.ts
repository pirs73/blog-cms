import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IMedia extends mongoose.Document {
  filename: string;
  mimetype: string;
  path: string;
}

const mediaSchema = new Schema({
  filename: { type: String },
  mimetype: { type: String },
  path: { type: String },
});

export default mongoose.model<IMedia>('Media', mediaSchema);
