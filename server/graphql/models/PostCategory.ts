import { PostCategoryFields } from '../../../interfaces';

class PostCategory {
  [x: string]: any;
  constructor(model: any) {
    this.Model = model;
  }

  async create(data: PostCategoryFields) {
    return await this.Model.create(data);
  }

  async getAll() {
    const res = await this.Model.find({});
    return res;
  }
}

export { PostCategory };
