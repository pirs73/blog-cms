import { TagFields } from '../../../interfaces';

class Tag {
  [x: string]: any;
  constructor(model: any) {
    this.Model = model;
  }

  async create(data: TagFields) {
    return await this.Model.create(data);
  }
}

export { Tag };
