import React, { ReactNode, useEffect, type JSX } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import withApollo from '@app/hoc/withApollo';
import { AdminMenu } from '@app/components';
import { useLazyViewerQuery } from '@app/apollo/actions';
import { connectWithSocketServer } from '@app/realtimeCommunication/socketConnection';
import 'antd/dist/reset.css';
import styles from './AdminLayout.module.css';

type Props = {
  children?: ReactNode;
  title?: string;
};

const AdminLayout = ({
  children,
  title = 'This is the default title',
}: Props): JSX.Element => {
  const [viewerQuery, { data, error }] = useLazyViewerQuery();
  useEffect(() => {
    viewerQuery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (error) {
    return <div>Error...</div>;
  }

  if (data) {
    if (data.viewer) {
      connectWithSocketServer(data.viewer.token);
    }
  }

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className={styles.adminLayout}>
        <header className={styles.adminLayout__header}>
          <Link href="/" className={styles.logo}>
            Site Title
          </Link>
        </header>
        <div className={styles.adminLayout__container}>
          <div className={styles.adminLayout__aside}>
            <AdminMenu />
          </div>
          <div className={styles.adminLayout__content}>
            {children}
            <footer>
              <hr className="mt-0" />
              <span>Im here to stay (Footer)</span>
            </footer>
          </div>
        </div>
      </div>
    </>
  );
};

AdminLayout.getServerSideProps = async (props: any) => {
  return { props };
};

export default withApollo(AdminLayout);
