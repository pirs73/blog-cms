import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { PostCategoryResponse } from '@app/interfaces';

export interface CratePostCategoryFormProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLFormElement>, HTMLFormElement> {
  postCategories: PostCategoryResponse[];
}
