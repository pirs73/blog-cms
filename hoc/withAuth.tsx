import { useGetUser } from '@app/apollo/actions';
import { Redirect } from '@app/components/shared';

interface WithAuthProps extends Record<string, unknown> {}

// eslint-disable-next-line import/no-anonymous-default-export
export default (WrappedComponent: any, role: string) =>
  (props: WithAuthProps) => {
    const {
      data: { user } = { user: '' },
      loading,
      error,
    } = useGetUser({ fetchPolicy: 'network-only' });

    if (!loading && (!user || error) && typeof window !== undefined) {
      return <Redirect to="/login" />;
    }

    if (user) {
      if (role && user.role !== role) {
        return <Redirect to="/login" />;
      }
      return <WrappedComponent {...props} />;
    }

    return <p>Authenticated...</p>;
  };
