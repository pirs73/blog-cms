import { SubmitHandler, useForm } from 'react-hook-form';
import { SignInDataArgs } from '@app/interfaces';

interface LoginFormProps {
  onSubmit: SubmitHandler<SignInDataArgs>;
  loading?: boolean;
}

const LoginForm = ({ onSubmit, loading }: LoginFormProps) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<SignInDataArgs>();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-3">
        <label htmlFor="email" className="form-label">
          Email
        </label>
        <input
          type="email"
          className="form-control"
          id="email"
          {...register('email', { required: true })}
        />
        {errors.email?.type === 'required' && (
          <span className="text-danger">email is required</span>
        )}
      </div>
      <div className="mb-3">
        <label htmlFor="password" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="password"
          {...register('password', { required: true })}
        />
        {errors.password?.type === 'required' && (
          <span className="text-danger">password is required</span>
        )}
      </div>
      {loading && <span>Singin in...</span>}
      {!loading && (
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      )}
    </form>
  );
};

export { LoginForm };
