/* eslint-disable @next/next/no-img-element */
import { BaseLayout } from '../../layouts';
import { LinkCategories } from '../../components';

const apiCall = () => {
  return new Promise((res, _rej) => {
    setTimeout(() => {
      res({ testData: 'test Data' });
    }, 200);
  });
};

const Posts = (props: any) => {
  return (
    <BaseLayout title="Posts" isFooter={true}>
      <section className="container">
        <h1 className="pt-3 pb-3">Posts</h1>
        {props.testData}
        <div className="row">
          <div className="col-md-8">
            <div className="card mb-3">
              <img src="" className="card-img-top" alt="" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
            <div className="card mb-3">
              <img src="" className="card-img-top" alt="" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <LinkCategories />
          </div>
        </div>
      </section>
    </BaseLayout>
  );
};

export async function getServerSideProps() {
  const data = await apiCall();
  return { props: data };
}

export default Posts;
