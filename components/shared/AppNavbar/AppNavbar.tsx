import { AppNavbarProps } from './AppNavbar.props';
import styles from './AppNavbar.module.css';

const AppNavbar = ({ children }: AppNavbarProps) => {
  return <div className={styles.appNavbar}>{children}</div>;
};

export { AppNavbar };
