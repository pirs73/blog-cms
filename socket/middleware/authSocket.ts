import Iron from '@hapi/iron';

const TOKEN_SECRET = process.env.TOKEN_SECRET || '';

async function verifyTokenSocket(socket: any, next: any) {
  const token = socket.handshake.auth?.token;
  try {
    const decoded = await Iron.unseal(token!, TOKEN_SECRET, Iron.defaults);
    socket.user = decoded;
  } catch (error) {
    console.error(error);
    const socketError = new Error('NOT_AUTHORIZED');

    return next(socketError);
  }

  next();
}

export default verifyTokenSocket;
