import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useApolloClient } from '@apollo/client';
import withApollo from '../hoc/withApollo';
import { useSignOut } from '../apollo/actions';

const Logout = () => {
  const [signOut] = useSignOut();
  const client = useApolloClient();
  const router = useRouter();

  useEffect(() => {
    signOut().then(() => {
      client.resetStore().then(() => router.push('/login'));
    });
  }, [signOut, router, client]);

  return (
    <>
      <div className="bwm-form mt-5">
        <div className="row">
          <div className="col-md-5 mx-auto">
            <h1 className="page-title">Logout</h1>
            <p>Signing out...</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default withApollo(Logout);
