import React from 'react';

import styles from './AdvertBlock.module.css';

const AdvertBlock = () => {
  return (
    <section className={styles.advertBlock}>
      <p>Your advertisement could be here</p>
    </section>
  );
};

export { AdvertBlock };
