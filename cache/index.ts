import { makeVar, InMemoryCache } from '@apollo/client';

export const submenusVar = makeVar([]);

export const pathNameVar = makeVar('');

export const onlineUsersVar = makeVar([]);

export const newPostCreateVar = makeVar([]);

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        submenus: {
          read() {
            return submenusVar();
          },
        },
        pathName: {
          read() {
            return pathNameVar();
          },
        },
        onlineUsers: {
          read() {
            return onlineUsersVar();
          },
        },
        newPostCreate: {
          read() {
            return newPostCreateVar();
          },
        },
        linkCategories: {
          merge(_existing: any, incoming: any) {
            // Equivalent to what happens if there is no custom merge function.
            return incoming;
          },
        },
      },
    },
  },
});
