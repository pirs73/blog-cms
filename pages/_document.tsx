import Document, { Html, Head, Main, NextScript } from 'next/document';

import type { JSX } from 'react';

class MyDocument extends Document {
  render(): JSX.Element {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="UTF-8" />
          <meta name="description" content="Sample description" />
          <meta name="keywords" content="HTML,CSS,TypeScript,JavaScript" />
          <meta name="author" content="John Doe" />
          <link
            rel="shortcut icon"
            href="/favicon.ico"
            type="image/x-icon"
          ></link>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
