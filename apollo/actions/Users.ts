import { useQuery } from '@apollo/client';
import { GET_ALL_USERS } from '@app/apollo/queries';

export const useGetAllUsers = () => useQuery(GET_ALL_USERS);
