const LinkCategoryFields = `
  name: String!
  description: String
  slug: String
`;

const LinkFields = `
  title: String!
  url: String!
  category: LinkCategory
`;

export const linksTypes = `
  type LinkCategory {
    _id: ID!
    ${LinkCategoryFields}
    links: [LinkResponse]
  }

  input LinkCategoryInput {
    ${LinkCategoryFields}
  }

  type LinkResponse {
    _id: ID!
    ${LinkFields}
  }
`;
