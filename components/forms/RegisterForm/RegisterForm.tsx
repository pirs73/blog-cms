import { SubmitHandler, useForm } from 'react-hook-form';
import { SignUpDataArgs } from '@app/interfaces';

interface RegisterFormProps {
  onSubmit: SubmitHandler<SignUpDataArgs>;
}

const RegisterForm = ({ onSubmit }: RegisterFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignUpDataArgs>();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-3">
        <label htmlFor="avatar" className="form-label">
          Avatar
        </label>
        <input
          type="text"
          className="form-control"
          id="avatar"
          {...register('avatar')}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="username" className="form-label">
          Username
        </label>
        <input
          type="text"
          className="form-control"
          id="username"
          {...register('username', { required: true })}
        />
        {errors.username?.type === 'required' && (
          <span className="text-danger">username is required</span>
        )}
      </div>
      <div className="mb-3">
        <label htmlFor="email" className="form-label">
          Email
        </label>
        <input
          type="email"
          className="form-control"
          id="email"
          {...register('email', { required: true })}
        />
        {errors.email?.type === 'required' && (
          <span className="text-danger">email is required</span>
        )}
      </div>
      <div className="mb-3">
        <label htmlFor="password" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="password"
          {...register('password', { required: true })}
        />
        {errors.password?.type === 'required' && (
          <span className="text-danger">password is required</span>
        )}
      </div>
      <div className="mb-3">
        <label htmlFor="passwordConfirmation" className="form-label">
          Password Confirmation
        </label>
        <input
          type="password"
          className="form-control"
          id="passwordConfirmation"
          {...register('passwordConfirmation', { required: true })}
        />
        {errors.passwordConfirmation?.type === 'required' && (
          <span className="text-danger">passwordConfirmation is required</span>
        )}
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export { RegisterForm };
