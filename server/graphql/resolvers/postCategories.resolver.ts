import { GraphQLError } from 'graphql';
import { PostCategoryFields, PostCategoryResponse } from '../../../interfaces';
import _ from 'lodash';

export const postCategoriesQueris = {
  postCategories: async (
    _root: undefined,
    _args: undefined,
    ctx: any,
    _info: undefined,
  ): Promise<PostCategoryResponse[]> => {
    try {
      return await ctx.models.PostCategory.getAll();
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};

export const postCategoriesMutations = {
  createPostCategory: async (
    _root: undefined,
    { name, slug, parentCategory, description }: PostCategoryFields,
    ctx: any,
    _info: undefined,
  ): Promise<PostCategoryResponse> => {
    const formatedSlug = _.kebabCase(slug);
    try {
      const createdPostCategory = await ctx.models.PostCategory.create({
        name,
        slug: formatedSlug,
        parentCategory,
        description,
      });

      if (!createdPostCategory) {
        throw new GraphQLError(`GraphQLError not server response`);
      } else {
        return createdPostCategory;
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
