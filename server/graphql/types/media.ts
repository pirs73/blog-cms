export const mediaTypes = `
  type File {
    _id: ID!
    filename: String!
    mimetype: String!
    path: String!
  }
`;
