import mongoose from 'mongoose';
import { ILink } from '../../../interfaces';

const { Schema } = mongoose;

export interface ILinkCategory extends mongoose.Document {
  name: string;
  description?: string;
  slug?: string;
  links?: ILink[];
}

const linkCategorySchema = new Schema({
  name: { type: String, required: true, maxlength: 128 },
  description: { type: String, maxlength: 512 },
  slug: { type: String, maxlength: 64 },
  links: [{ type: Schema.Types.ObjectId, ref: 'Link' }],
});

export default mongoose.model<ILinkCategory>(
  'LinkCategory',
  linkCategorySchema,
);
