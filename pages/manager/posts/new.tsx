import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const PostCreate = () => {
  useEffect(() => {
    pathNameVar('/manager/posts/new/');
  }, []);

  return (
    <AdminLayout title="Create Post">
      <h1>Create Post</h1>
    </AdminLayout>
  );
};

export default PostCreate;
