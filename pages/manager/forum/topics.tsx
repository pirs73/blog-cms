import React, { useEffect } from 'react';
import { AdminLayout } from '../../../layouts';
import { pathNameVar } from '../../../cache';

const ForumTopics = () => {
  useEffect(() => {
    pathNameVar('/manager/forum/topics/');
  }, []);

  return (
    <AdminLayout title="Forum Topics">
      <h1>Forum Topics</h1>
    </AdminLayout>
  );
};

export default ForumTopics;
