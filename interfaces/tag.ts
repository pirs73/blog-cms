export interface TagFields {
  name: string;
  description?: string;
  slug: string;
}

export interface TagResponse extends TagFields {
  _id: string;
}

export interface Tags {
  tags: TagResponse[];
}
