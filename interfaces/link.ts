export interface LinkFields {
  title: string;
  url: string;
  category: string;
}

export interface ILink extends LinkFields {
  _id: string;
  createdAt: string;
  updatedAt: string;
}
