import { useQuery } from '@apollo/client';
import { GET_LINKS } from '../queries';

export const useGetLinks = () => useQuery(GET_LINKS);
