/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { useForm } from 'react-hook-form';
import withApollo from '@app/hoc/withApollo';
import { useCreateLinkCategory } from '@app/apollo/actions';
import { LinkCategoryFields } from '@app/interfaces';

const CrateLinkCategoryForm = () => {
  const [createLinkCategory, { error }] = useCreateLinkCategory();
  // const errorMessage = (error) => {
  //   return (
  //     (error.graphQLErrors && error.graphQLErrors[0].message) ||
  //     'Ooooops something went wrong...'
  //   );
  // };

  const { handleSubmit, register } = useForm<LinkCategoryFields>({
    defaultValues: {},
  });

  const onSubmit = async (data: LinkCategoryFields) => {
    const { name, description, slug } = data;
    await createLinkCategory({ variables: { name, description, slug } });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input
          {...register('name')}
          type="text"
          className="form-control"
          id="name"
        />
      </div>
      <div className="form-group">
        <label htmlFor="description">Description</label>
        <input
          {...register('description')}
          type="text"
          className="form-control"
          id="description"
        />
      </div>
      <div className="form-group">
        <label htmlFor="slug">slug</label>
        <input
          {...register('slug')}
          type="text"
          className="form-control"
          id="slug"
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default withApollo(CrateLinkCategoryForm);
