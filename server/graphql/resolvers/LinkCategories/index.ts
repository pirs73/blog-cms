 
import { LinkCategoryArgs, LinkCategoryInputArgs } from './types';

export const linkCategoriesQueris = {
  linkCategory: async (
    _root: undefined,
    { id }: LinkCategoryArgs,
    ctx: any,
  ) => {
    return await ctx.models.LinkCategory.getById(id);
  },
  linkCategories: async (_root: undefined, _args: any, ctx: any) => {
    return await ctx.models.LinkCategory.getAll();
  },
};

export const linkCategoriesMutations = {
  createLinkCategory: async (
    _root: undefined,
    { name, description, slug }: any,
    ctx: any,
  ) => {
    const createdLinkCategory = await ctx.models.LinkCategory.create({
      name,
      description,
      slug,
    });
    return createdLinkCategory;
  },
  updateLinkCategory: async (
    _root: undefined,
    { id, input }: LinkCategoryInputArgs,
    ctx: any,
  ) => {
    const updatedLinkCategory = await ctx.models.LinkCategory.findAndUpdate(
      { _id: id },
      input,
      // { new: true },
    );

    if (updatedLinkCategory && updatedLinkCategory.modifiedCount) {
      return true;
    } else {
      return false;
    }
  },
  deleteLinkCategory: async (
    _root: undefined,
    { id }: LinkCategoryArgs,
    ctx: any,
  ) => {
    const deletedLinkCategoryId = await ctx.models.LinkCategory.findAndDelete({
      _id: id,
    });

    return deletedLinkCategoryId;
  },
};
