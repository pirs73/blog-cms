 
import {
  LinkCategoryArgs,
  LinkCategoryInputArgs,
} from '../resolvers/LinkCategories/types';
class LinkCategory {
  [x: string]: any;
  constructor(model: any) {
    this.Model = model;
  }
  async getAll() {
    const res = await this.Model.find({}).populate('links');
    return res;
  }

  async getById(id: LinkCategoryArgs) {
    return await this.Model.findById(id).populate('links');
  }

  async create(data: LinkCategoryInputArgs) {
    return await this.Model.create(data);
  }

  async findAndUpdate(id: LinkCategoryArgs, data: LinkCategoryInputArgs) {
    return await this.Model.updateOne({ _id: id }, { ...data });
  }

  async findAndDelete(id: LinkCategoryArgs) {
    const deletedLinkCategory = await this.Model.deleteOne({
      _id: id,
    });

    if (!deletedLinkCategory.deletedCount) {
      return null;
    }
    return deletedLinkCategory._id;
  }
}

export { LinkCategory };
