import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface AppNavbarProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  children: ReactNode;
}
