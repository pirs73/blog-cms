 
 
import React from 'react';
import Link from 'next/link';
import { useGetLinkCategories } from '@app/apollo/actions';
import { LinkCategory } from '@app/interfaces';

import style from './LinkCategories.module.css';

const useInitialData = () => {
  const { data } = useGetLinkCategories();
  const linkCategories: LinkCategory[] = (data && data.linkCategories) || [];

  return {
    linkCategories,
  };
};

const LinkCategories = () => {
  const { linkCategories } = useInitialData();
  return (
    <div className={style.linkCategories}>
      <h5>Links</h5>
      {linkCategories &&
        linkCategories.map((linkCategory: any) => (
          <div key={linkCategory._id}>
            <h6>{linkCategory.name}</h6>
            {linkCategory.links && linkCategory.links.length > 0 && (
              <ul>
                {linkCategory.links.map((l: any) => (
                  <li key={l._id}>
                    <Link href={l.url} title={l.title}>
                      {l.title}
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </div>
        ))}
    </div>
  );
};

export default LinkCategories;
