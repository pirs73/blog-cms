import React, { useEffect } from 'react';
import withApollo from '@app/hoc/withApollo';
import { pathNameVar } from '@app/cache';
import { AdminLayout } from '@app/layouts';
import { useGetLinks } from '@app/apollo/actions';

const Links = () => {
  const { error, data } = useGetLinks();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const links = (data && data.links) || [];

  useEffect(() => {
    pathNameVar('/manager/links/');
  }, []);

  if (error) return <div>`Error! ${error.message}`</div>;

  return (
    <AdminLayout title="Links">
      <h1>Links</h1>
    </AdminLayout>
  );
};

export default withApollo(Links);
