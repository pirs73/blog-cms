import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface ILink extends mongoose.Document {
  title: string;
  url: string;
  category?: string;
}

const linkSchema = new Schema(
  {
    title: { type: String, required: true, maxlength: 128 },
    url: { type: String, required: true, maxlength: 512 },
    category: { type: Schema.Types.ObjectId, ref: 'LinkCategory' },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<ILink>('Link', linkSchema);
