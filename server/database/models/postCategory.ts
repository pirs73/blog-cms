import mongoose from 'mongoose';

const { Schema } = mongoose;

export interface IPostCategory extends mongoose.Document {
  name: string;
  slug: string;
  parentCategory?: string;
  description?: string;
}

const postCategorySchema = new Schema(
  {
    name: { type: String, required: true, maxlength: 128 },
    description: { type: String, maxlength: 512 },
    slug: {
      type: String,
      required: true,
      lowercase: true,
      maxlength: 64,
      unique: true,
    },
    parentCategory: {
      type: Schema.Types.ObjectId,
      ref: 'PostCategory',
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IPostCategory>(
  'PostCategory',
  postCategorySchema,
);
