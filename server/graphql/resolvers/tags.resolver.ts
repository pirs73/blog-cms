import { GraphQLError } from 'graphql';
import { TagFields, TagResponse } from '../../../interfaces';

export const tagsMutations = {
  createTag: async (
    _root: undefined,
    { name, description, slug }: TagFields,
    ctx: any,
    _info: undefined,
  ): Promise<TagResponse> => {
    try {
      const createdTag = await ctx.models.Tag.create({
        name,
        description,
        slug,
      });

      if (!createdTag) {
        throw new GraphQLError(`GraphQLError not server response`);
      } else {
        return createdTag;
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
