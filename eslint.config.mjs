import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { FlatCompat } from '@eslint/eslintrc';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const compat = new FlatCompat({
  baseDirectory: __dirname,
});

const eslintConfig = [
  ...compat.extends('next/core-web-vitals', 'next/typescript'),
  {
    ignores: [
      '**/node_modules/',
      '**/out/',
      '**/.next/',
      '**/dist/',
      '**/volumes/',
      '**/__tests__/',
      'jest.config.js',
      'next.config.js',
      'postcss.config.js',
    ],
  },
  {
    rules: {
      indent: 'off',
      '@next/next/no-document-import-in-page': 'off',
      // Disable prop-types as we use TypeScript for type checking
      'react/prop-types': 'off',
      'react/display-name': 'off',
      '@typescript-eslint/indent': 'off',
      '@typescript-eslint/explicit-function-return-type': 'off',
      '@typescript-eslint/interface-name-prefix': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/explicit-module-boundary-types': 'off',
      '@typescript-eslint/no-empty-object-type': 'off',
      '@typescript-eslint/no-empty-interface': [
        'error',
        {
          allowSingleExtends: true,
        },
      ],
      '@typescript-eslint/no-unused-vars': [
        2,
        {
          argsIgnorePattern: '^_',
        },
      ],
      'no-console': [
        2,
        {
          allow: ['warn', 'error', 'info'],
        },
      ],

      // needed for NextJS's jsx without react import
      'react/react-in-jsx-scope': 'off',
    },
  },
];

export default eslintConfig;
