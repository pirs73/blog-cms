import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IPost extends mongoose.Document {
  title: string;
  content: any;
  categories?: string[];
  tags?: string[];
  published: boolean;
  postedBy: string;
  slug: string;
}

const postSchema = new Schema(
  {
    title: { type: String, required: true, maxlength: 128 },
    content: {},
    categories: [{ type: Schema.Types.ObjectId, ref: 'PostCategory' }],
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    published: { type: Boolean, default: true },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User' },
    slug: {
      type: String,
      unique: true,
      required: true,
      lowercase: true,
      maxlength: 64,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IPost>('Post', postSchema);
