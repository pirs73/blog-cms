import { GraphQLUpload } from 'graphql-upload-minimal';
import {
  linkCategoriesQueris,
  linkCategoriesMutations,
} from './LinkCategories';
import { linkQueris } from './Link';
import { mediaMutations } from './Media';
import {
  postCategoriesMutations,
  postCategoriesQueris,
} from './postCategories.resolver';
import { tagsMutations } from './tags.resolver';
import { userMutations, userQueries } from './User';

export const resolvers = {
  Upload: GraphQLUpload,
  Query: {
    ...userQueries,
    ...linkCategoriesQueris,
    ...linkQueris,
    ...postCategoriesQueris,
  },
  Mutation: {
    ...linkCategoriesMutations,
    ...mediaMutations,
    ...postCategoriesMutations,
    ...tagsMutations,
    ...userMutations,
  },
};
