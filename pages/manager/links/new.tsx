import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const LinkCreate = () => {
  useEffect(() => {
    pathNameVar('/manager/links/new/');
  }, []);

  return (
    <AdminLayout title="Create Link">
      <h1>Create Link</h1>
    </AdminLayout>
  );
};

export default LinkCreate;
