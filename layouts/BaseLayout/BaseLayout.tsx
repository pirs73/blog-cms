import { ReactNode, type JSX } from 'react';
import Head from 'next/head';
import withApollo from '@app/hoc/withApollo';
import { TopMenu } from '../../components';

import styles from './BaseLayout.module.css';

type Props = {
  children?: ReactNode;
  title?: string;
  isFooter?: boolean;
};

const BaseLayout = ({
  children,
  title = 'Blog CMS',
  isFooter = false,
}: Props): JSX.Element => {
  return (
    <div className={styles.baseLayout}>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <header className={styles.baseLayout__header}>
        <TopMenu />
      </header>
      <div className={styles.baseLayout__content}>
        {children}
        {isFooter && (
          <footer>
            <hr />
            <span>Im here to stay (Footer)</span>
          </footer>
        )}
      </div>
    </div>
  );
};

export default withApollo(BaseLayout);
