import { gql } from '@apollo/client';
import { linksTypes, mediaTypes, postTypes, userTypes } from './types';

export const typeDefs = gql`
  ${linksTypes}
  ${mediaTypes}
  ${postTypes}
  ${userTypes}

  scalar Upload

  type Query {
    user(id: ID!): User!
    users: [User]!
    viewer: ViewerResponse
    linkCategory: LinkCategory!
    linkCategories: [LinkCategory!]!

    links: [LinkResponse]
    linksByCategory: [LinkResponse]

    postCategories: [PostCategoryResponse!]!

    files: [File!]
  }

  type Mutation {
    createLinkCategory(
      name: String
      description: String
      slug: String
    ): LinkCategory!
    updateLinkCategory(id: ID, input: LinkCategoryInput!): Boolean
    deleteLinkCategory(id: ID): ID

    createPostCategory(
      name: String!
      slug: String!
      parentCategory: String
      description: String
    ): PostCategoryResponse!

    createTag(name: String!, description: String, slug: String!): TagResponse!

    signUp(input: SignUpInput!): SignUpPayload!
    signIn(input: SignInInput!): SignInPayload!
    signOut: Boolean

    uploadFile(file: Upload!): File!
  }
`;
