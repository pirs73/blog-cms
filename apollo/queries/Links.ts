import { gql } from '@apollo/client';

export const GET_LINKS = gql`
  query Links {
    links {
      _id
      title
      url
      category {
        name
      }
    }
  }
`;
