import React from 'react';
import cn from 'classnames';
import { ContainerProps } from './Container.props';
import styles from './Container.module.css';

const Container = ({ children, className, ...props }: ContainerProps) => {
  return (
    <div className={cn(styles.container, className)} {...props}>
      {children}
    </div>
  );
};

export { Container };
