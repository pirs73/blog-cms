/* eslint-disable no-console */
import React from 'react';
import { Button, Table } from 'antd';
import withApollo from '@app/hoc/withApollo';
import { CratePostCategoryForm } from '@app/components';
import { useGetPostCategories } from '@app/apollo/actions';
import { PostCategoryResponse } from '@app/interfaces';

const useInitialData = () => {
  const { data } = useGetPostCategories();
  const postCategories: PostCategoryResponse[] =
    (data && data.postCategories) || [];

  return {
    postCategories,
  };
};

const PostCategories = () => {
  const { postCategories: data } = useInitialData();
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Slug',
      dataIndex: 'slug',
      key: 'slug',
    },
    {
      title: 'Update',
      key: 'update',
      render: (_text: any, record: { _id: string }) => (
        <Button danger size="small" onClick={() => console.log(record._id)}>
          Update
        </Button>
      ),
    },
    {
      title: 'Delete',
      key: 'delete',
      render: (_text: any, record: { _id: string }) => (
        <Button
          type="primary"
          danger
          size="small"
          onClick={() => console.log(record._id)}
        >
          Delete
        </Button>
      ),
    },
  ];
  return (
    <div className={`admin-content row`}>
      <div className="col-md-4">
        <h5>Add New Post Category</h5>
        <CratePostCategoryForm postCategories={data} />
      </div>
      <div className="col-md-8">
        <Table
          columns={columns}
          dataSource={data}
          rowKey="_id"
          pagination={false}
        />
      </div>
    </div>
  );
};

export default withApollo(PostCategories);
