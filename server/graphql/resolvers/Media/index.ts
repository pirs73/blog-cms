import { format } from 'date-fns';
import { createWriteStream, mkdir } from 'fs';

import Media from '../../../database/models/media';

const storeUpload = async ({
  stream,
  filename,
  mimetype,
}: {
  stream: any;
  filename: string;
  mimetype: string;
}) => {
  const id = format(new Date(), 'dd-MM-yyyy_hh-mm-ss_SSS');
  const path = `public/uploads/${id}-${filename}`;

  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on('finish', () => resolve({ id, path, filename, mimetype }))
      .on('error', reject),
  );
};

const processUpload = async (upload: any) => {
  const { createReadStream, filename, mimetype } = await upload;
  const stream = createReadStream();
  const file = await storeUpload({ stream, filename, mimetype });
  return file;
};

export const mediaMutations = {
  uploadFile: async (_root: undefined, { file }: any) => {
    mkdir('public/uploads', { recursive: true }, (err) => {
      if (err) throw err;
    });

    const upload: any = await processUpload(file);
    delete upload.id;
    await Media.create(upload);
    return upload;
  },
};
