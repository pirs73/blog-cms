export interface PostCategoryFields {
  name: string;
  slug: string;
  parentCategory?: string | null;
  description?: string;
}

export interface PostCategoryResponse extends PostCategoryFields {
  _id: string;
  createdAt: string;
  updatedAt: string;
}

// export interface PostCategories {
//   postCategories: PostCategoryResponse[];
// }
