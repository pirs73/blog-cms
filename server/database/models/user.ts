import mongoose from 'mongoose';
import { RoleTypes } from '../../../interfaces';

export interface IUserSchema extends mongoose.Document {
  avatar?: string;
  email: string;
  name?: string;
  username: string;
  password: string;
  salt: string;
  role: RoleTypes;
  info?: string;
}

const userSchema = new mongoose.Schema(
  {
    /** TODO: avatar переделать чтоб выбирался из медиатеки */
    avatar: String,
    email: {
      type: String,
      required: [true, 'Email is required'],
      lowercase: true,
      index: true,
      unique: true,
      match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    name: {
      type: String,
      minlength: [3, 'Minimum name length is 3 characters!'],
    },
    username: {
      type: String,
      required: true,
      minlength: [3, 'Minimum username length is 3 characters!'],
    },
    password: {
      type: String,
      required: true,
    },
    salt: {
      type: String,
      required: true,
    },
    role: {
      enum: ['guest', 'admin', 'manager'],
      type: String,
      required: true,
      default: 'guest',
    },
    info: String,
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IUserSchema>('User', userSchema);
