import React, { useState, useEffect } from 'react';
import { Button, Table } from 'antd';
import { LinkCategory } from '@app/interfaces';
import { CrateLinkCategoryForm } from '@app/components';

// import style from './AdminLinkCategories.module.css';

interface IFuncArgs {
  variables: {
    id: string;
  };
}
interface IProps {
  data: LinkCategory[];
  updateLinkCategory(args: IFuncArgs): void;
  deleteLinkCategory(args: IFuncArgs): void;
}

const AdminLinkCategories = ({
  data,
  updateLinkCategory,
  deleteLinkCategory,
}: IProps) => {
  const [dataSource, setDataSource] = useState<LinkCategory[]>([]);

  useEffect(() => {
    setDataSource(data);

    return () => {
      setDataSource(data);
    };
  }, [data]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Slug',
      dataIndex: 'slug',
      key: 'slug',
    },
    {
      title: 'Update',
      key: 'update',
      render: (_text: any, record: { _id: string }) => (
        <Button
          danger
          size="small"
          onClick={() => updateLinkCategory({ variables: { id: record?._id } })}
        >
          Update
        </Button>
      ),
    },
    {
      title: 'Delete',
      key: 'delete',
      render: (_text: any, record: { _id: string }) => (
        <Button
          type="primary"
          danger
          size="small"
          onClick={() => deleteLinkCategory({ variables: { id: record._id } })}
        >
          Delete
        </Button>
      ),
    },
  ];

  return (
    <div className={`admin-content row`}>
      <div className="col-md-4">
        <h5>Add New Link Category</h5>
        <CrateLinkCategoryForm />
      </div>
      <div className="col-md-8">
        <Table
          columns={columns}
          dataSource={dataSource}
          rowKey="_id"
          pagination={false}
        />
      </div>
    </div>
  );
};

AdminLinkCategories.getServerSideProps = async (props: IProps) => {
  return { props };
};

export { AdminLinkCategories };
