import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const PostsAdmin = () => {
  useEffect(() => {
    pathNameVar('/manager/posts/');
  }, []);

  return (
    <AdminLayout title="All Posts">
      <h1>All Posts</h1>
    </AdminLayout>
  );
};

export default PostsAdmin;
