import React from 'react';
import Image from 'next/image';
// import { useRouter } from 'next/router';
import { BaseLayout } from '../../layouts';

const PostDetail = ({ query }: any) => {
  // const router = useRouter();
  const { id } = query;
  return (
    <BaseLayout title="Posts">
      <section className="container">
        <h1 className="pt-3 pb-3">Post ID: {id}</h1>
        <div className="row">
          <div className="col-12">
            <div className="card mb-3">
              <Image
                alt="Vercel logo"
                src="/images/horde.jpg"
                width={1920}
                height={1080}
              />
              <div className="card-body">
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </BaseLayout>
  );
};

PostDetail.getInitialProps = ({ query }: any) => {
  return { query };
};

export default PostDetail;
