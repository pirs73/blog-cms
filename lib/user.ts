import crypto from 'crypto';
import User from '../server/database/models/user';
import { IUser, SignUpDataArgs } from '../interfaces';

export async function createUser({
  avatar,
  username,
  email,
  password,
  passwordConfirmation,
}: SignUpDataArgs) {
  if (password !== passwordConfirmation) {
    throw new Error('Password must be the same as confirmation password!');
  }
  // Here you should create the user and save the salt and hashed password (some dbs may have
  // authentication methods that will do it for you so you don't have to worry about it):
  const salt = crypto.randomBytes(16).toString('hex');
  const hash = crypto
    .pbkdf2Sync(password, salt, 1000, 64, 'sha512')
    .toString('hex');
  const user = {
    avatar,
    username,
    email,
    password: hash,
    hash,
    salt,
    role: 'guest',
  };

  // This is an in memory store for users, there is no data persistence without a proper DB
  try {
    const newUser = await User.create(user);
    return newUser;
  } catch (error: any) {
    if (error && error.code && error.code === 11000) {
      throw new Error('User with provided email already exists!');
    }

    throw error;
  }
}

export async function findUser({ email }: { email: string }) {
  // This is an in memory store for users, there is no data persistence without a proper DB
  const user = await User.findOne({ email });
  return user;
}

// Compare the password of an already fetched user (using `findUser`) and compare the
// password for a potential match
export function validatePassword(user: IUser, inputPassword: string) {
  const inputHash = crypto
    .pbkdf2Sync(inputPassword, user.salt, 1000, 64, 'sha512')
    .toString('hex');
  const passwordsMatch = user.password === inputHash;

  return passwordsMatch;
}
