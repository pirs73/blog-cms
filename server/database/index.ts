/* eslint-disable @typescript-eslint/no-unused-expressions */
import mongoose from 'mongoose';
import { connectDatabaseOptions } from '../database/utils';
import { MONGO_URI } from '../constants';
import linkCategory from './models/linkCategory';
import link from './models/link';
import media from './models/media';
import postCategory from './models/postCategory';
import tag from './models/tag';
import user from './models/user';

linkCategory;
link;
media;
postCategory;
tag;
user;

export const connectDatabase = async (): Promise<void> => {
  try {
    mongoose.set('strictQuery', false);
    await mongoose.connect(MONGO_URI, connectDatabaseOptions);
    console.info('Mongodb connected');
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};
