 
 
import { ReactNode } from 'react';
import Link from 'next/link';
import { useReactiveVar } from '@apollo/client';
import withApollo from '../../hoc/withApollo';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import { submenusVar, pathNameVar } from '../../cache';
import styles from './AdminMenu.module.css';

type MenuItem = Required<MenuProps>['items'][number];

type AppLinkProps = {
  children?: ReactNode;
  className?: string;
  href: string;
};

const AppLink = ({ children, className, href }: AppLinkProps) => (
  <Link href={href} className={className}>
    {children}
  </Link>
);

function getItem(
  label: React.ReactNode,
  key: React.Key,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    children,
    label,
    type,
  } as MenuItem;
}

const AdminMenu = () => {
  const submenus = useReactiveVar(submenusVar);
  const patchname = useReactiveVar(pathNameVar);

  const handleMenuClick = (openKeysArray: string[]) => {
    submenusVar(openKeysArray);
  };

  const items: MenuItem[] = [
    getItem('Posts', 'sub1', [
      getItem(
        <AppLink href="/manager/posts/" className={styles.adminMenu__link}>
          All Posts
        </AppLink>,
        '/manager/posts/',
      ),
      getItem(
        <AppLink href="/manager/posts/new/" className={styles.adminMenu__link}>
          Add New
        </AppLink>,
        '/manager/posts/new/',
      ),
      getItem(
        <AppLink href="/manager/categories/" className={styles.adminMenu__link}>
          Categories
        </AppLink>,
        '/manager/categories/',
      ),
      getItem(
        <AppLink href="/manager/tags/" className={styles.adminMenu__link}>
          Tags
        </AppLink>,
        '/manager/tags/',
      ),
    ]),

    getItem('Links', 'sub2', [
      getItem(
        <AppLink href="/manager/links/" className={styles.adminMenu__link}>
          All Links
        </AppLink>,
        '/manager/links/',
      ),
      getItem(
        <AppLink href="/manager/links/new/" className={styles.adminMenu__link}>
          Add New
        </AppLink>,
        '/manager/links/new/',
      ),
      getItem(
        <AppLink
          href="/manager/links/categories/"
          className={styles.adminMenu__link}
        >
          Link Categories
        </AppLink>,
        '/manager/links/categories/',
      ),
    ]),
    getItem('Media', 'sub3', [
      getItem(
        <AppLink href="/manager/media/" className={styles.adminMenu__link}>
          Library
        </AppLink>,
        '/manager/media/',
      ),
      getItem(
        <AppLink href="/manager/media/new/" className={styles.adminMenu__link}>
          Add New
        </AppLink>,
        '/manager/media/new/',
      ),
    ]),
    getItem('Forum', 'sub4', [
      getItem(
        <AppLink
          href="/manager/forum/categories/"
          className={styles.adminMenu__link}
        >
          Categories
        </AppLink>,
        '/manager/forum/categories/',
      ),
      getItem(
        <AppLink
          href="/manager/forum/topics/"
          className={styles.adminMenu__link}
        >
          Topics
        </AppLink>,
        '/manager/forum/topics/',
      ),
    ]),
    getItem('Users', 'sub5', [
      getItem(
        <AppLink href="/manager/users/" className={styles.adminMenu__link}>
          All Users
        </AppLink>,
        '/manager/users/',
      ),
      getItem(
        <AppLink href="/manager/users/new/" className={styles.adminMenu__link}>
          Add New
        </AppLink>,
        '/manager/users/new/',
      ),
      getItem(
        <AppLink href="/manager/users/1/" className={styles.adminMenu__link}>
          Profile
        </AppLink>,
        '/manager/users/1/',
      ),
    ]),
    getItem('Settings', 'sub6', [
      getItem(
        <AppLink href="/manager/settings/" className={styles.adminMenu__link}>
          General
        </AppLink>,
        '/manager/settings/',
      ),
    ]),
  ];

  return (
    <div className={styles.adminMenu}>
      <Menu
        mode="inline"
        theme="dark"
        items={items}
        openKeys={submenus}
        selectedKeys={[patchname]}
        onOpenChange={(openKeys) => handleMenuClick(openKeys)}
      />
      {/* <Menu
        theme={'dark'}
        style={{ width: 256 }}
        defaultOpenKeys={['sub1']}
        openKeys={submenus}
        selectedKeys={[patchname]}
        mode="inline"
      >
        <SubMenu key="sub6" title="Settings" onTitleClick={handleSubMenuClick}>
          <Menu items={settingsItems} theme={'dark'} />
        </SubMenu>
      </Menu> */}
    </div>
  );
};

const adminMenu = withApollo(AdminMenu);
export { adminMenu as AdminMenu };
