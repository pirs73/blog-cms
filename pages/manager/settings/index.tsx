import React, { useEffect } from 'react';
import { pathNameVar } from '../../../cache';
import { AdminLayout } from '../../../layouts';

const SettingsGeneral = () => {
  useEffect(() => {
    pathNameVar('/manager/settings/');
  }, []);

  return (
    <AdminLayout title="Settings General">
      <h1>Settings General</h1>
    </AdminLayout>
  );
};

export default SettingsGeneral;
