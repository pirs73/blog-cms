import express, { Express } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import path from 'path';
import { graphqlUploadExpress } from 'graphql-upload-minimal';

export const init = (server: Express) => {
  const allowedOrigins = [
    `http://localhost:3000`,
    `http://localhost:4000`,
    `https://studio.apollographql.com`,
  ];

  const dir = path.join(process.cwd(), 'public/uploads');
  console.info(dir);

  server.use(
    cors<cors.CorsRequest>({
      origin: function (origin, callback) {
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
          const msg =
            'The CORS policy for this site does not ' +
            'allow access from the specified Origin.';
          return callback(new Error(msg), false);
        }
        return callback(null, true);
      },
      credentials: true,
    }),
    bodyParser.json(),
  );

  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(
    graphqlUploadExpress({
      maxFileSize: 10000000, // 10 MB
      maxFiles: 20,
    }),
  );

  server.use(express.static(dir));
  server.use('public/uploads', express.static(dir));
};
