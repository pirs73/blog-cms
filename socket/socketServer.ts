import * as socketio from 'socket.io';
import verifyTokenSocket from './middleware/authSocket';
import { newConnectionHandler } from './socketHandlers/newConnectionHandler';
import { disconnectHandler } from './socketHandlers/disconnectHandler';

const registerSocketServer = (server: any) => {
  const io: socketio.Server = new socketio.Server();
  io.attach(server, {
    cors: {
      origin: [
        `http://localhost`,
        `http://localhost:3000`,
        `https://studio.apollographql.com`,
      ],
      methods: ['GET', 'POST', 'OPTIONS'],
    },
  });

  io.use((socket: any, next: any) => {
    verifyTokenSocket(socket, next);
  });

  io.on('connection', (socket: socketio.Socket) => {
    console.info('user connected');
    console.info(socket.id);

    newConnectionHandler(socket, io);

    socket.on('disconnect', () => {
      disconnectHandler(socket);
    });

    // socket.emit('status', 'Hello from Socket.io');
  });
};

export default registerSocketServer;
