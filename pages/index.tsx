/* eslint-disable @next/next/no-img-element */
import React, { type JSX } from 'react';
import { BaseLayout, Container } from '../layouts';
import { AdvertBlock, LinkCategories } from '../components';

const IndexPage = (): JSX.Element => (
  <>
    <BaseLayout title="Bloggg CMS" isFooter={true}>
      <AdvertBlock />
      <Container>
        <h1 className="pt-3 pb-3">Welcome to the Site Title</h1>
        <div className="row">
          <div className="col-md-8">
            <div className="card mb-3">
              <img src="" className="card-img-top" alt="" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
            <div className="card mb-3">
              <img src="" className="card-img-top" alt="" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
            <div className="card mb-3">
              <img src="" className="card-img-top" alt="" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the cards content.
                </p>
              </div>
              <div className="card-body">
                <a href="#" className="card-link">
                  Card link
                </a>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <LinkCategories />
          </div>
        </div>
      </Container>
    </BaseLayout>
  </>
);

export default IndexPage;
