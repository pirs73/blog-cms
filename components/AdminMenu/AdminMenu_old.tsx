import { KeyboardEvent, MouseEvent, ReactNode } from 'react';
import Link from 'next/link';
import { useReactiveVar } from '@apollo/client';
import withApollo from '../../hoc/withApollo';
import { Menu } from 'antd';
import { submenusVar, pathNameVar } from '../../cache';
import styles from './AdminMenu.module.css';

type AppLinkProps = {
  children?: ReactNode;
  className?: string;
  href: string;
};

interface TitleEventEntity {
  key: string;
  domEvent: MouseEvent<HTMLElement> | KeyboardEvent<HTMLElement>;
}

const { SubMenu } = Menu;

const AppLink = ({ children, className, href }: AppLinkProps) => (
  <Link href={href} className={className}>
    {children}
  </Link>
);

const postsItems = [
  {
    label: (
      <AppLink href="/manager/posts/" className={styles.adminMenu__link}>
        All Posts
      </AppLink>
    ),
    key: '/manager/posts/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/posts/new/" className={styles.adminMenu__link}>
        Add New
      </AppLink>
    ),
    key: '/manager/posts/new/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/categories/" className={styles.adminMenu__link}>
        Categories
      </AppLink>
    ),
    key: '/manager/categories/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/tags/" className={styles.adminMenu__link}>
        Tags
      </AppLink>
    ),
    key: '/manager/tags/',
    className: styles.menuItem,
  },
];

const linksItems = [
  {
    label: (
      <AppLink href="/manager/links/" className={styles.adminMenu__link}>
        All Links
      </AppLink>
    ),
    key: '/manager/links/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/links/new/" className={styles.adminMenu__link}>
        Add New
      </AppLink>
    ),
    key: '/manager/links/new/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink
        href="/manager/links/categories/"
        className={styles.adminMenu__link}
      >
        Link Categories
      </AppLink>
    ),
    key: '/manager/links/categories/',
    className: styles.menuItem,
  },
];

const mediaItems = [
  {
    label: (
      <AppLink href="/manager/media/" className={styles.adminMenu__link}>
        Library
      </AppLink>
    ),
    key: '/manager/media/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/media/new/" className={styles.adminMenu__link}>
        Add New
      </AppLink>
    ),
    key: '/manager/media/new/',
    className: styles.menuItem,
  },
];

const forumItems = [
  {
    label: (
      <AppLink
        href="/manager/forum/categories/"
        className={styles.adminMenu__link}
      >
        Categories
      </AppLink>
    ),
    key: '/manager/forum/categories/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/forum/topics/" className={styles.adminMenu__link}>
        Topics
      </AppLink>
    ),
    key: '/manager/forum/topics/',
    className: styles.menuItem,
  },
];

const usersItems = [
  {
    label: (
      <AppLink href="/manager/users/" className={styles.adminMenu__link}>
        All Users
      </AppLink>
    ),
    key: '/manager/users/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/users/new/" className={styles.adminMenu__link}>
        Add New
      </AppLink>
    ),
    key: '/manager/users/new/',
    className: styles.menuItem,
  },
  {
    label: (
      <AppLink href="/manager/users/1/" className={styles.adminMenu__link}>
        Profile
      </AppLink>
    ),
    key: '/manager/users/[id]/',
    className: styles.menuItem,
  },
];

const settingsItems = [
  {
    label: (
      <AppLink href="/manager/settings/" className={styles.adminMenu__link}>
        General
      </AppLink>
    ),
    key: '/manager/settings/',
    className: styles.menuItem,
  },
];

const AdminMenu = () => {
  const submenus = useReactiveVar(submenusVar);
  const patchname = useReactiveVar(pathNameVar);

  const handleSubMenuClick = (e: TitleEventEntity) => {
    const subKey = submenusVar().filter((s: string) => s === e.key.toString());
    if (subKey.length > 0) {
      const newSubmenus = submenusVar().filter(
        (s: string) => s !== e.key.toString(),
      );
      submenusVar(newSubmenus);
    } else {
      submenusVar([...submenusVar(), e.key]);
    }
  };
  return (
    <div className={styles.adminMenu}>
      <Menu
        theme={'dark'}
        style={{ width: 256 }}
        defaultOpenKeys={['sub1']}
        openKeys={submenus}
        selectedKeys={[patchname]}
        mode="inline"
      >
        <SubMenu key="sub1" title="Posts" onTitleClick={handleSubMenuClick}>
          <Menu items={postsItems} theme={'dark'} />
        </SubMenu>
        <SubMenu key="sub2" title="Links" onTitleClick={handleSubMenuClick}>
          <Menu items={linksItems} theme={'dark'} />
        </SubMenu>
        <SubMenu key="sub3" title="Media" onTitleClick={handleSubMenuClick}>
          <Menu items={mediaItems} theme={'dark'} />
        </SubMenu>
        <SubMenu key="sub4" title="Forum" onTitleClick={handleSubMenuClick}>
          <Menu items={forumItems} theme={'dark'} />
        </SubMenu>
        <SubMenu key="sub5" title="Users" onTitleClick={handleSubMenuClick}>
          <Menu items={usersItems} theme={'dark'} />
        </SubMenu>
        <SubMenu key="sub6" title="Settings" onTitleClick={handleSubMenuClick}>
          <Menu items={settingsItems} theme={'dark'} />
        </SubMenu>
      </Menu>
    </div>
  );
};

const adminMenu = withApollo(AdminMenu);
export { adminMenu as AdminMenu };
