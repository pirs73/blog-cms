import { addNewConnectedUser } from '../serverStore';

export const newConnectionHandler = (socket: any, _io: any) => {
  const userDetails = socket.user;

  addNewConnectedUser({
    socketId: socket.id,
    userId: userDetails.id,
  });
};
