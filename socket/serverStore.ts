const connectedUsers = new Map();

export const addNewConnectedUser = ({ socketId, userId }: any) => {
  connectedUsers.set(socketId, { userId });
  console.info('new connected users');
  console.info(connectedUsers);
};

export const removeConnectedUser = (socketId: any) => {
  if (connectedUsers.has(socketId)) {
    connectedUsers.delete(socketId);
    console.info('new disconnected users');
    console.info(connectedUsers);
  }
};
