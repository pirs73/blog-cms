import React, { useCallback } from 'react';
import Image from 'next/image';
import { useMutation, gql } from '@apollo/client';
import { useDropzone } from 'react-dropzone';
import styles from './FileUpload.module.css';

export const UploadMutation = gql`
  mutation uploadFile($file: Upload!) {
    uploadFile(file: $file) {
      path
    }
  }
`;

const FileUpload = () => {
  const [uploadFile, { data }] = useMutation(UploadMutation);
  const image = (data && data.uploadFile) || null;

  const onDrop = useCallback(
    (acceptedFiles: any) => {
      const file = acceptedFiles[0];
      uploadFile({
        variables: { file },
        /** TODO: сделать оповещение о загрузке */
        onCompleted: () => console.info('Upload complete'),
      });
    },
    [uploadFile],
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const normFile = (e: any) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };

  return (
    <>
      {image && image.path && (
        <Image
          src={`/${image.path.slice(7)}`}
          alt={`image ${image.path.slice(14)}}`}
          width={200}
          height={200}
          className={styles.image}
        />
      )}
      <div
        {...getRootProps()}
        className={`dropzone ${isDragActive && 'isActive'}`}
      >
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <p className={styles.text}>
            Drag n drop some files here, or click to select files
          </p>
        )}
      </div>
    </>
  );
};

export { FileUpload };
